﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSumFormApp.Model
{
    public class Profile
    {
        public string projectName { get; set; }
        public List<string> destination { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", projectName);
        }
    }

    public class ProjectProfile
    {
        public List<Profile> profiles { get; set; }
    }
}
