﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSumFormApp.Model
{
    public class FileDetail
    {
        public string Name { get; set; }
        public string DateModified { get; set; }
    }
}
