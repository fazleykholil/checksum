﻿using CheckSumFormApp.Model;
using CheckSumFormApp.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CheckSumFormApp
{
    public partial class FormCustomSettings : Form
    {
        private const string projectSettingsIdentifier = "ProjectNameSettings_";
        private static List<string> deploymentsPath = new List<string>();
        private static List<string> projectNames = new List<string>();
        public FormCustomSettings()
        {
            InitializeComponent();
        }

        private void FormCustomSettings_Load(object sender, EventArgs e)
        {
           

        }



        private void btAddProjectPath_Click(object sender, EventArgs e)
        {
            
        }

        private string BuildMultipleProjectPath()
        {
            string pathsStr = "";
            foreach (string path in deploymentsPath)
                pathsStr += path + ";";
            return pathsStr;
        }

        private void btConfirmAddProject_Click(object sender, EventArgs e)
        {
         
        }

        private void RefreshDataSource(ListBox listBox, List<string> list)
        {
            listBox.DataSource = null;
            listBox.DataSource = list;
        }
    }
}