﻿using Checksum.Factory;
using CheckSumFormApp.Model;
using CheckSumFormApp.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CheckSumFormApp
{
    public partial class Form1 : Form
    {
        IZipFactory ZipFactory = ZipServiceFactory.Instance();
        private static List<string> profile = null;
        private static List<string> filesDeployed = new List<string>();
        private static int isBackupSucessful = 0;
        private static bool isBackedUp = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lstDeployments.DataSource = null;
            pnlLoadFromFile.Visible = true;
            rbReadFromFile.Checked = true;
            pnlEnterManually.Visible = false;
            LoadProjectProfiles();

        }

        private void LoadProjectProfiles()
        {

            string json = File.ReadAllText(ConfigurationManager.AppSettings["profile"]);
            ProjectProfile projectProfile = JsonConvert.DeserializeObject<ProjectProfile>(json);
            lstDeployments.DataSource = projectProfile.profiles;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BrowseFolder(txtSource);
        }

        private void BrowseFolder(TextBox txtBox)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //string[] files = Directory.GetFiles(folderBrowserDialog1.SelectedPath);
                txtBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btZip_Click(object sender, EventArgs e)
        {
            if (txtSource.Text != "" && txtDestination.Text != "")
            {
                pnlLoad.Visible = true;
                Thread zipThread = new Thread(Zip);
                zipThread.Start();
            }
            else
                MessageBox.Show("Error please enter source and destination correctly");
        }

        private void Zip()
        {
            string output = ZipFactory
                 .GetZipService()
                 .Zip(txtSource.Text, txtDestination.Text);
            if (txtConsole.InvokeRequired)
                this.Invoke(new MethodInvoker(() => txtConsole.Text = output));
            else
                txtConsole.Text = output;
            if (pnlLoad.InvokeRequired)
                this.Invoke(new MethodInvoker(() => pnlLoad.Visible = false));
            else
                pnlLoad.Visible = false;
        }


        private void Unzip()
        {
            foreach (string destination in profile)
            {
                try
                {
                    string output = ZipFactory.GetZipService().UnZip(txtZipFile.Text, destination);

                    if (txtConsole.InvokeRequired)
                        this.Invoke(new MethodInvoker(() => txtConsole.AppendText(output)));
                    else
                        txtConsole.Text = output;
                    List<string> dirs = FormUtils.DirSearch(destination);
                    foreach (string file in dirs)
                    {
                        FileInfo fileInfo = new FileInfo(file);
                        filesDeployed.Add(file + "......." + fileInfo.LastAccessTime);
                    }

                    Invoke(new MethodInvoker(() => txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage(destination + ": deployed sucessfully"))));
                }
                catch (Exception e)
                {
                    Invoke(new MethodInvoker(() => txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage("File deployment error:" + destination + "\r\n" + e.Message))));
                }
            }
            if (pnlLoad.InvokeRequired)
                this.Invoke(new MethodInvoker(() => pnlLoad.Visible = false));
            else
                pnlLoad.Visible = false;
            treeView1.PathSeparator = @"\";
            Invoke(new MethodInvoker(() => FormUtils.PopulateTreeView(treeView1, filesDeployed, '\\')));
            Invoke(new MethodInvoker(() => txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage("All files deployed sucessfully"))));

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            pnlEnterManually.Visible = false;
            pnlLoadFromFile.Visible = true;
        }

        private void btCompare_Click(object sender, EventArgs e)
        {
            if (txtHashValue1.Text != "" && txtHashValue2.Text != "")
            {
                if (txtHashValue1.Text == txtHashValue2.Text)
                    txtConsole.AppendText(ConsoleOutput.ConsOutputCheckSumString(true));
                else
                    txtConsole.AppendText(ConsoleOutput.ConsOutputCheckSumString(false));
            }
            else
                MessageBox.Show("Enter both hash values to be able to compare.");

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            pnlEnterManually.Visible = true;
            pnlLoadFromFile.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtHashValueFromFile1.Text != "" && txtHashValueFromFile2.Text != "")
            {
                if (txtHashValueFromFile1.Text == txtHashValueFromFile2.Text)
                    txtConsole.AppendText(ConsoleOutput.ConsOutputCheckSumString(true));
                else
                    txtConsole.AppendText(ConsoleOutput.ConsOutputCheckSumString(false));
            }
            else
                MessageBox.Show("Select both files to be able to compare");
        }

        private void btOpenFile1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                txtHashValueFromFile1.Text = ReadHashValue(file);
            }
        }

        private string ReadHashValue(string file)
        {
            try
            {
                return File.ReadAllText(file);

            }
            catch (IOException)
            {
                return "";
            }
        }

        private void btOpenFile2_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                string destinationHashValue = ZipFactory.GetZipService().checksum(file);
                txtHashValueFromFile2.Text = destinationHashValue;
            }
        }

        private void btDestination_Click(object sender, EventArgs e)
        {
            BrowseFolder(txtDestination);
        }

        private void btUnzip_Click(object sender, EventArgs e)
        {
            if (txtZipFile.Text != "" && profile != null)
            {
                pnlLoad.Visible = true;
                Thread zipThread = new Thread(Unzip);
                zipThread.Start();
            }
            else
                MessageBox.Show("Error please select Zip file and destination correctly");

        }



        private void rbReadFromFile_CheckedChanged_1(object sender, EventArgs e)
        {
            pnlLoadFromFile.Visible = true;
            pnlEnterManually.Visible = false;
        }

        private void rbManually_CheckedChanged(object sender, EventArgs e)
        {
            pnlEnterManually.Visible = true;
            pnlLoadFromFile.Visible = false;

        }

        private void btBackup_Click(object sender, EventArgs e)
        {
            isBackedUp = false;
            isBackupSucessful = 0;
            if (profile != null && txtZipFile.Text != "")
            {
                foreach (string file in profile)
                {
                    if (!isBackedUp)
                    {
                        string dirName = Path.GetFileName(file.TrimEnd(Path.DirectorySeparatorChar));
                        string backupDestination = txtZipFile.Text + @"\..\..\backup\" + dirName +"_"+ DateTime.Now.ToString("yyyy_MM_dd_HH.mm.ss"); ;
                        bool result = FormUtils.DirectoryCopy(file, backupDestination, true);
                        if (result)
                        {
                            txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage(string.Format("backup sucessfull: {0}", backupDestination)));
                            isBackedUp = true;
                        }
                        else
                        {
                            isBackupSucessful++;
                            txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage(string.Format("backup Unsucessfull: {0}", file)));
                        }
                    }
                }
                if (isBackupSucessful == 0)
                {
                    txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage(string.Format("Backup done...\r\n Proceed to deployment...")));
                    btUnzip.Enabled = true;
                }
                else
                    txtConsole.AppendText(ConsoleOutput.ConsOutStandardMessage(string.Format("Backup Error...\r\nPlease retry to be able to proceed...")));
            }
            else
            {
                MessageBox.Show("Error please select both the compress file and a project");
                btChooseZipFile.Focus();
            }
        }

        private void btSettings_Click(object sender, EventArgs e)
        {
            //FormCustomSettings formCustomSettings = new FormCustomSettings();
            //formCustomSettings.ShowDialog();
        }

        private void btChooseZipFile_Click_1(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                txtZipFile.Text = file;
            }
        }

        private void lstDeployments_DoubleClick(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Confirm project", "Important!", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                Profile p = (Profile)lstDeployments.SelectedItem;
                profile = p.destination;
                btBackup.Enabled = true;
                lblSelectedProject.Text = p.projectName;
            }
        }
    }
}
