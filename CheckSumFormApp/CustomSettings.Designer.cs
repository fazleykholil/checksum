﻿namespace CheckSumFormApp
{
    partial class FormCustomSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstProjects = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.btAddProjectPath = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btConfirmAddProject = new System.Windows.Forms.Button();
            this.lstProjectPath = new System.Windows.Forms.ListBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstProjects
            // 
            this.lstProjects.FormattingEnabled = true;
            this.lstProjects.HorizontalScrollbar = true;
            this.lstProjects.Location = new System.Drawing.Point(12, 12);
            this.lstProjects.Name = "lstProjects";
            this.lstProjects.Size = new System.Drawing.Size(350, 160);
            this.lstProjects.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Project name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Deployment paths";
            // 
            // txtProjectName
            // 
            this.txtProjectName.Location = new System.Drawing.Point(101, 28);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(239, 20);
            this.txtProjectName.TabIndex = 5;
            // 
            // btAddProjectPath
            // 
            this.btAddProjectPath.Location = new System.Drawing.Point(98, 60);
            this.btAddProjectPath.Name = "btAddProjectPath";
            this.btAddProjectPath.Size = new System.Drawing.Size(24, 23);
            this.btAddProjectPath.TabIndex = 6;
            this.btAddProjectPath.Text = "+";
            this.btAddProjectPath.UseVisualStyleBackColor = true;
            this.btAddProjectPath.Click += new System.EventHandler(this.btAddProjectPath_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstProjectPath);
            this.groupBox1.Controls.Add(this.btConfirmAddProject);
            this.groupBox1.Controls.Add(this.txtProjectName);
            this.groupBox1.Controls.Add(this.btAddProjectPath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 178);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 225);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new Project";
            // 
            // btConfirmAddProject
            // 
            this.btConfirmAddProject.Location = new System.Drawing.Point(282, 197);
            this.btConfirmAddProject.Name = "btConfirmAddProject";
            this.btConfirmAddProject.Size = new System.Drawing.Size(58, 23);
            this.btConfirmAddProject.TabIndex = 7;
            this.btConfirmAddProject.Text = "Add";
            this.btConfirmAddProject.UseVisualStyleBackColor = true;
            this.btConfirmAddProject.Click += new System.EventHandler(this.btConfirmAddProject_Click);
            // 
            // lstProjectPath
            // 
            this.lstProjectPath.FormattingEnabled = true;
            this.lstProjectPath.HorizontalScrollbar = true;
            this.lstProjectPath.Location = new System.Drawing.Point(9, 90);
            this.lstProjectPath.Name = "lstProjectPath";
            this.lstProjectPath.Size = new System.Drawing.Size(331, 95);
            this.lstProjectPath.TabIndex = 8;
            // 
            // FormCustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 410);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstProjects);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormCustomSettings";
            this.Text = "CustomSettings";
            this.Load += new System.EventHandler(this.FormCustomSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstProjects;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.Button btAddProjectPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btConfirmAddProject;
        private System.Windows.Forms.ListBox lstProjectPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}