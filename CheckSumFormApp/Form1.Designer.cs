﻿namespace CheckSumFormApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btDestination = new System.Windows.Forms.Button();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btZip = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLoad = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.pbLoad = new System.Windows.Forms.ProgressBar();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtConsole = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnlLoadFromFile = new System.Windows.Forms.Panel();
            this.txtHashValueFromFile2 = new System.Windows.Forms.TextBox();
            this.txtHashValueFromFile1 = new System.Windows.Forms.TextBox();
            this.btOpenFile2 = new System.Windows.Forms.Button();
            this.btOpenFile1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlEnterManually = new System.Windows.Forms.Panel();
            this.btCompare = new System.Windows.Forms.Button();
            this.txtHashValue2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHashValue1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbManually = new System.Windows.Forms.RadioButton();
            this.rbReadFromFile = new System.Windows.Forms.RadioButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtZipFile = new System.Windows.Forms.TextBox();
            this.btChooseZipFile = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btBackup = new System.Windows.Forms.Button();
            this.lstDeployments = new System.Windows.Forms.ListBox();
            this.btUnzip = new System.Windows.Forms.Button();
            this.lblTreeTitle = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btSettings = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSelectedProject = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.pnlLoad.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlLoadFromFile.SuspendLayout();
            this.pnlEnterManually.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btDestination);
            this.groupBox1.Controls.Add(this.txtDestination);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btZip);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtSource);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(390, 109);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zip Files";
            // 
            // btDestination
            // 
            this.btDestination.Location = new System.Drawing.Point(334, 58);
            this.btDestination.Name = "btDestination";
            this.btDestination.Size = new System.Drawing.Size(28, 23);
            this.btDestination.TabIndex = 8;
            this.btDestination.Text = "...";
            this.btDestination.UseVisualStyleBackColor = true;
            this.btDestination.Click += new System.EventHandler(this.btDestination_Click);
            // 
            // txtDestination
            // 
            this.txtDestination.Enabled = false;
            this.txtDestination.Location = new System.Drawing.Point(77, 60);
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.Size = new System.Drawing.Size(251, 20);
            this.txtDestination.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Destination";
            // 
            // btZip
            // 
            this.btZip.Location = new System.Drawing.Point(287, 87);
            this.btZip.Name = "btZip";
            this.btZip.Size = new System.Drawing.Size(75, 23);
            this.btZip.TabIndex = 4;
            this.btZip.Text = "Zip";
            this.btZip.UseVisualStyleBackColor = true;
            this.btZip.Click += new System.EventHandler(this.btZip_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(334, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSource
            // 
            this.txtSource.Enabled = false;
            this.txtSource.Location = new System.Drawing.Point(77, 29);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(251, 20);
            this.txtSource.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Source";
            // 
            // pnlLoad
            // 
            this.pnlLoad.Controls.Add(this.label7);
            this.pnlLoad.Controls.Add(this.pbLoad);
            this.pnlLoad.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlLoad.Location = new System.Drawing.Point(0, 624);
            this.pnlLoad.Name = "pnlLoad";
            this.pnlLoad.Size = new System.Drawing.Size(777, 35);
            this.pnlLoad.TabIndex = 5;
            this.pnlLoad.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(208, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Please wait while files are being processed";
            // 
            // pbLoad
            // 
            this.pbLoad.Location = new System.Drawing.Point(9, 20);
            this.pbLoad.Name = "pbLoad";
            this.pbLoad.Size = new System.Drawing.Size(741, 10);
            this.pbLoad.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbLoad.TabIndex = 6;
            // 
            // txtConsole
            // 
            this.txtConsole.BackColor = System.Drawing.SystemColors.Info;
            this.txtConsole.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtConsole.Location = new System.Drawing.Point(408, 29);
            this.txtConsole.Multiline = true;
            this.txtConsole.Name = "txtConsole";
            this.txtConsole.ReadOnly = true;
            this.txtConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtConsole.Size = new System.Drawing.Size(357, 314);
            this.txtConsole.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pnlLoadFromFile);
            this.groupBox2.Controls.Add(this.pnlEnterManually);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(12, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(390, 207);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compare Checksum";
            // 
            // pnlLoadFromFile
            // 
            this.pnlLoadFromFile.Controls.Add(this.txtHashValueFromFile2);
            this.pnlLoadFromFile.Controls.Add(this.txtHashValueFromFile1);
            this.pnlLoadFromFile.Controls.Add(this.btOpenFile2);
            this.pnlLoadFromFile.Controls.Add(this.btOpenFile1);
            this.pnlLoadFromFile.Controls.Add(this.button3);
            this.pnlLoadFromFile.Controls.Add(this.label6);
            this.pnlLoadFromFile.Controls.Add(this.label5);
            this.pnlLoadFromFile.Location = new System.Drawing.Point(8, 91);
            this.pnlLoadFromFile.Name = "pnlLoadFromFile";
            this.pnlLoadFromFile.Size = new System.Drawing.Size(375, 102);
            this.pnlLoadFromFile.TabIndex = 3;
            // 
            // txtHashValueFromFile2
            // 
            this.txtHashValueFromFile2.Enabled = false;
            this.txtHashValueFromFile2.Location = new System.Drawing.Point(95, 41);
            this.txtHashValueFromFile2.Name = "txtHashValueFromFile2";
            this.txtHashValueFromFile2.Size = new System.Drawing.Size(245, 20);
            this.txtHashValueFromFile2.TabIndex = 8;
            // 
            // txtHashValueFromFile1
            // 
            this.txtHashValueFromFile1.Enabled = false;
            this.txtHashValueFromFile1.Location = new System.Drawing.Point(95, 14);
            this.txtHashValueFromFile1.Name = "txtHashValueFromFile1";
            this.txtHashValueFromFile1.Size = new System.Drawing.Size(245, 20);
            this.txtHashValueFromFile1.TabIndex = 7;
            // 
            // btOpenFile2
            // 
            this.btOpenFile2.Location = new System.Drawing.Point(346, 41);
            this.btOpenFile2.Name = "btOpenFile2";
            this.btOpenFile2.Size = new System.Drawing.Size(28, 23);
            this.btOpenFile2.TabIndex = 6;
            this.btOpenFile2.Text = "...";
            this.btOpenFile2.UseVisualStyleBackColor = true;
            this.btOpenFile2.Click += new System.EventHandler(this.btOpenFile2_Click);
            // 
            // btOpenFile1
            // 
            this.btOpenFile1.Location = new System.Drawing.Point(346, 12);
            this.btOpenFile1.Name = "btOpenFile1";
            this.btOpenFile1.Size = new System.Drawing.Size(28, 23);
            this.btOpenFile1.TabIndex = 5;
            this.btOpenFile1.Text = "...";
            this.btOpenFile1.UseVisualStyleBackColor = true;
            this.btOpenFile1.Click += new System.EventHandler(this.btOpenFile1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(299, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Compare";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Zip file";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "checksum txt file";
            // 
            // pnlEnterManually
            // 
            this.pnlEnterManually.Controls.Add(this.btCompare);
            this.pnlEnterManually.Controls.Add(this.txtHashValue2);
            this.pnlEnterManually.Controls.Add(this.label4);
            this.pnlEnterManually.Controls.Add(this.txtHashValue1);
            this.pnlEnterManually.Controls.Add(this.label3);
            this.pnlEnterManually.Location = new System.Drawing.Point(6, 90);
            this.pnlEnterManually.Name = "pnlEnterManually";
            this.pnlEnterManually.Size = new System.Drawing.Size(378, 111);
            this.pnlEnterManually.TabIndex = 5;
            // 
            // btCompare
            // 
            this.btCompare.Location = new System.Drawing.Point(300, 79);
            this.btCompare.Name = "btCompare";
            this.btCompare.Size = new System.Drawing.Size(75, 23);
            this.btCompare.TabIndex = 2;
            this.btCompare.Text = "Compare";
            this.btCompare.UseVisualStyleBackColor = true;
            this.btCompare.Click += new System.EventHandler(this.btCompare_Click);
            // 
            // txtHashValue2
            // 
            this.txtHashValue2.Location = new System.Drawing.Point(86, 44);
            this.txtHashValue2.Name = "txtHashValue2";
            this.txtHashValue2.Size = new System.Drawing.Size(289, 20);
            this.txtHashValue2.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Hash value 2";
            // 
            // txtHashValue1
            // 
            this.txtHashValue1.Location = new System.Drawing.Point(86, 15);
            this.txtHashValue1.Name = "txtHashValue1";
            this.txtHashValue1.Size = new System.Drawing.Size(289, 20);
            this.txtHashValue1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Hash value 1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbManually);
            this.groupBox3.Controls.Add(this.rbReadFromFile);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(378, 65);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // rbManually
            // 
            this.rbManually.AutoSize = true;
            this.rbManually.Location = new System.Drawing.Point(6, 42);
            this.rbManually.Name = "rbManually";
            this.rbManually.Size = new System.Drawing.Size(149, 17);
            this.rbManually.TabIndex = 2;
            this.rbManually.TabStop = true;
            this.rbManually.Text = "Enter hash value manually";
            this.rbManually.UseVisualStyleBackColor = true;
            this.rbManually.CheckedChanged += new System.EventHandler(this.rbManually_CheckedChanged);
            // 
            // rbReadFromFile
            // 
            this.rbReadFromFile.AutoSize = true;
            this.rbReadFromFile.Location = new System.Drawing.Point(6, 19);
            this.rbReadFromFile.Name = "rbReadFromFile";
            this.rbReadFromFile.Size = new System.Drawing.Size(90, 17);
            this.rbReadFromFile.TabIndex = 3;
            this.rbReadFromFile.Text = "Read from file";
            this.rbReadFromFile.UseVisualStyleBackColor = true;
            this.rbReadFromFile.CheckedChanged += new System.EventHandler(this.rbReadFromFile_CheckedChanged_1);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.lblTreeTitle);
            this.groupBox4.Controls.Add(this.treeView1);
            this.groupBox4.Location = new System.Drawing.Point(12, 349);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(753, 268);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Deployments";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.lblSelectedProject);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.txtZipFile);
            this.groupBox5.Controls.Add(this.btChooseZipFile);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.btBackup);
            this.groupBox5.Controls.Add(this.lstDeployments);
            this.groupBox5.Controls.Add(this.btUnzip);
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(362, 241);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            // 
            // txtZipFile
            // 
            this.txtZipFile.Enabled = false;
            this.txtZipFile.Location = new System.Drawing.Point(65, 22);
            this.txtZipFile.Name = "txtZipFile";
            this.txtZipFile.Size = new System.Drawing.Size(229, 20);
            this.txtZipFile.TabIndex = 13;
            // 
            // btChooseZipFile
            // 
            this.btChooseZipFile.Location = new System.Drawing.Point(300, 22);
            this.btChooseZipFile.Name = "btChooseZipFile";
            this.btChooseZipFile.Size = new System.Drawing.Size(27, 23);
            this.btChooseZipFile.TabIndex = 12;
            this.btChooseZipFile.Text = "...";
            this.btChooseZipFile.UseVisualStyleBackColor = true;
            this.btChooseZipFile.Click += new System.EventHandler(this.btChooseZipFile_Click_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Select gz";
            // 
            // btBackup
            // 
            this.btBackup.Enabled = false;
            this.btBackup.Location = new System.Drawing.Point(278, 171);
            this.btBackup.Name = "btBackup";
            this.btBackup.Size = new System.Drawing.Size(75, 23);
            this.btBackup.TabIndex = 10;
            this.btBackup.Text = "Backup";
            this.btBackup.UseVisualStyleBackColor = true;
            this.btBackup.Click += new System.EventHandler(this.btBackup_Click);
            // 
            // lstDeployments
            // 
            this.lstDeployments.FormattingEnabled = true;
            this.lstDeployments.Location = new System.Drawing.Point(6, 70);
            this.lstDeployments.Name = "lstDeployments";
            this.lstDeployments.Size = new System.Drawing.Size(347, 95);
            this.lstDeployments.TabIndex = 9;
            this.lstDeployments.DoubleClick += new System.EventHandler(this.lstDeployments_DoubleClick);
            // 
            // btUnzip
            // 
            this.btUnzip.Enabled = false;
            this.btUnzip.Location = new System.Drawing.Point(278, 200);
            this.btUnzip.Name = "btUnzip";
            this.btUnzip.Size = new System.Drawing.Size(75, 23);
            this.btUnzip.TabIndex = 2;
            this.btUnzip.Text = "Deploy";
            this.btUnzip.UseVisualStyleBackColor = true;
            this.btUnzip.Click += new System.EventHandler(this.btUnzip_Click);
            // 
            // lblTreeTitle
            // 
            this.lblTreeTitle.AutoSize = true;
            this.lblTreeTitle.Location = new System.Drawing.Point(393, 16);
            this.lblTreeTitle.Name = "lblTreeTitle";
            this.lblTreeTitle.Size = new System.Drawing.Size(37, 13);
            this.lblTreeTitle.TabIndex = 8;
            this.lblTreeTitle.Text = "Files ..";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(396, 33);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(351, 227);
            this.treeView1.TabIndex = 7;
            // 
            // btSettings
            // 
            this.btSettings.BackgroundImage = global::CheckSumFormApp.Properties.Resources.very_basic_settings_icon;
            this.btSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSettings.Location = new System.Drawing.Point(752, 0);
            this.btSettings.Name = "btSettings";
            this.btSettings.Size = new System.Drawing.Size(27, 23);
            this.btSettings.TabIndex = 6;
            this.btSettings.UseVisualStyleBackColor = true;
            this.btSettings.Click += new System.EventHandler(this.btSettings_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Double click to select project";
            // 
            // lblSelectedProject
            // 
            this.lblSelectedProject.AutoSize = true;
            this.lblSelectedProject.ForeColor = System.Drawing.Color.Red;
            this.lblSelectedProject.Location = new System.Drawing.Point(94, 171);
            this.lblSelectedProject.Name = "lblSelectedProject";
            this.lblSelectedProject.Size = new System.Drawing.Size(33, 13);
            this.lblSelectedProject.TabIndex = 15;
            this.lblSelectedProject.Text = "None";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Selected project:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 659);
            this.Controls.Add(this.btSettings);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtConsole);
            this.Controls.Add(this.pnlLoad);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Checksum Application";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlLoad.ResumeLayout(false);
            this.pnlLoad.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.pnlLoadFromFile.ResumeLayout(false);
            this.pnlLoadFromFile.PerformLayout();
            this.pnlEnterManually.ResumeLayout(false);
            this.pnlEnterManually.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btZip;
        private System.Windows.Forms.TextBox txtConsole;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel pnlEnterManually;
        private System.Windows.Forms.TextBox txtHashValue1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHashValue2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btCompare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlLoadFromFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btOpenFile2;
        private System.Windows.Forms.Button btOpenFile1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtHashValueFromFile2;
        private System.Windows.Forms.TextBox txtHashValueFromFile1;
        private System.Windows.Forms.ProgressBar pbLoad;
        private System.Windows.Forms.Panel pnlLoad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btDestination;
        private System.Windows.Forms.TextBox txtDestination;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btUnzip;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label lblTreeTitle;
        private System.Windows.Forms.ListBox lstDeployments;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbManually;
        private System.Windows.Forms.RadioButton rbReadFromFile;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btBackup;
        private System.Windows.Forms.Button btSettings;
        private System.Windows.Forms.TextBox txtZipFile;
        private System.Windows.Forms.Button btChooseZipFile;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblSelectedProject;
    }
}

