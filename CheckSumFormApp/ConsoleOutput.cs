﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSumFormApp
{
    public class ConsoleOutput
    {
        public static string ConsOutputCheckSumString(bool isValid)
        {
            string output = "";
            if (isValid)
            {
                output += "\r\n=======================================================";
                output += "\r\nFiles are valid";
                output += "\r\n=======================================================";
            }
            else
            {
                output += "\r\n=======================================================";
                output += "\r\nFiles are not valid";
                output += "\r\n=======================================================";
            }
            return output;
        }
        public static string ConsOutStandardMessage(string message)
        {
            string output = "";
            output += "\r\n=======================================================";
            output += "\r\n" + message;
            output += "\r\n=======================================================";
            return output;
        }
    }
}
