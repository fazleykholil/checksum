﻿using Checksum.Utils;

namespace Checksum.Services
{
    public class CheckSumService
    {
        public bool CompareFromFile(string filePath1, string filePath2)
        {
            string file1HashVlaue = FileUtils.ReadFromFile(filePath1);
            string file2HashVlaue = FileUtils.ReadFromFile(filePath2);

            if (file1HashVlaue == file2HashVlaue)
                return true;
            else
                return false;
        }
    }
}
