﻿using Checksum.Core;
using Checksum.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checksum.Services
{
    public class ZipService
    {
        public string Zip(string sourceBaseDirectoryPath, string destinationBasePath)
        {
            return BundleFiles(sourceBaseDirectoryPath, destinationBasePath);
        }

        public string UnZip(string sourceFile, string destinationPath)
        {
            StringBuilder line = new StringBuilder();
            string _7zipCommand = new CommandBuilder()
               .Build(sourceFile, "unzip", destinationPath)
               .CommandText;

            using (Process proc = new Process())
            {
                ProcessStartInfo p = proc.StartInfo;
                p.FileName = "7za.exe";
                p.Arguments = _7zipCommand;
                p.UseShellExecute = false;
                p.RedirectStandardOutput = true;
                p.CreateNoWindow = true;
                proc.Start();

                Console.WriteLine("Please wait while files are being uncompressed...");

                while (!proc.StandardOutput.EndOfStream)
                {
                    line.AppendLine(proc.StandardOutput.ReadLine());
                    Console.WriteLine(line);
                }

                proc.WaitForExit();
            }
            return line.ToString();
        }

        private string BundleFiles(string sourceName, string destinationBasePath)
        {
            StringBuilder line = new StringBuilder();
            string sourceFilePath = FileUtils.CompressFileName(sourceName);
            FileUtils.RemoveFileIfExist(sourceName);

            string _7zipCommand = new CommandBuilder()
               .Build(sourceName, "zip", null)
               .OverrideFiles(true)
               .CommandText;

            using (Process proc = new Process())
            {
                ProcessStartInfo p = proc.StartInfo;
                p.FileName = "7za.exe";
                p.Arguments = _7zipCommand;
                p.UseShellExecute = false;
                p.RedirectStandardOutput = true;
                p.CreateNoWindow = true;
                proc.Start();

                Console.WriteLine("Please wait while files are being compressed...");

                while (!proc.StandardOutput.EndOfStream)
                {
                    line.AppendLine(proc.StandardOutput.ReadLine());
                    Console.WriteLine(line);
                }

                proc.WaitForExit();
            }

            string hashValueSource = CheckSumAlgo.GetMD5HashFromFile(sourceFilePath);
            string checksumFileName = "checksum.txt";
            string generatedFilePath = FileUtils.WriteToFile(sourceName, checksumFileName, hashValueSource);
            string generatedCheckSumFileName = Path.GetFileName(generatedFilePath);
            string generatedZipFileName = Path.GetFileName(sourceFilePath);

            try
            {
                FileUtils.MoveWithReplace(generatedFilePath, destinationBasePath + @"\" + generatedCheckSumFileName);
                FileUtils.MoveWithReplace(sourceFilePath, destinationBasePath + @"\" + generatedZipFileName);
            }
            catch (Exception e)
            {
                line.AppendLine(string.Format("Error moving files to destination:{0}", e.Message));
            }

            return line.ToString();
        }

        public string checksum(string filePath)
        {
            return CheckSumAlgo.GetMD5HashFromFile(filePath);
        }

    }
}
