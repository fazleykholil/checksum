﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checksum.Utils
{
    public class CommandBuilder
    {
        public string CommandText { get; set; }
        public CommandBuilder Build(string sourceName, string type, string destination)
        {
            if (type == "zip")
            {
                string targetName = FileUtils.CompressFileName(sourceName);
                CommandText = "a -mx=9 -tzip " + targetName + " " + sourceName+@"\*";
            }
            else if (type == "unzip")
                CommandText = "x -r -y " + sourceName + " -o" + destination;
        

            return this;
        }

        public CommandBuilder OverrideFiles(bool overrideArchive)
        {
            if (overrideArchive == true)
            {
                CommandText += " -aoa";
                Console.WriteLine("Overwritting all existing files {0}", CommandText);

            }
            else
                Console.WriteLine("Overwritting files disabled");
            return this;
        }


    }
}
