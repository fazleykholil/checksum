﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checksum.Utils
{
    public class FileUtils
    {
        public static bool RemoveFileIfExist(string fileName)
        {
            if (File.Exists(CompressFileName(fileName)))
            {
                Console.WriteLine("{0} Exist", fileName);
                Console.WriteLine("Removing {0}", fileName);
                File.Delete(CompressFileName(fileName));
                return true;
            }
            else
                return false;
        }

        public static string CompressFileName(string fileName)
        {
            return fileName + ".gz";
        }

        public static string WriteToFile(string basePath, string fileName, string value)
        {
            string file = basePath + fileName;
            using (StreamWriter writer = new StreamWriter(basePath + fileName))
            {
                writer.Write(value);
            }
            return file;
        }

        public static string ReadFromFile(string fileName)
        {
            string value = "";
            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    value = reader.ReadLine();
                    reader.Close();
                }

            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }

            return value;
        }

        public static void DummyCopyFileToServer(string fileName, string destinationName)
        {
            try
            {
                File.Copy(fileName, destinationName, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void MoveWithReplace(string sourceFileName, string destFileName)
        {

            //first, delete target file if exists, as File.Move() does not support overwrite
            if (File.Exists(destFileName))
            {
                File.Delete(destFileName);
            }

            File.Move(sourceFileName, destFileName);

        }

    }
}
