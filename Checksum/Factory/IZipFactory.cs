﻿using Checksum.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checksum.Factory
{
    public interface IZipFactory
    {
        ZipService GetZipService();
        CheckSumService GetCheckSumService();
    }
}
