﻿using Checksum.Services;

namespace Checksum.Factory
{
    public class ZipServiceFactory : IZipFactory
    {
        private  static ZipService ZipService;
        private  static CheckSumService CheckSumService;
        private  static IZipFactory instance;

        private ZipServiceFactory() { }
        public static IZipFactory Instance()
        {
            if (null == instance)
            {
                instance = new ZipServiceFactory();
                ZipService = new ZipService();
                CheckSumService = new CheckSumService();
            }
            return instance;
        }
       
        public ZipService GetZipService()
        {
            return ZipService;
        }

        public CheckSumService GetCheckSumService()
        {
            return CheckSumService;
        }
    }
}
