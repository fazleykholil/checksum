﻿using Checksum.Factory;
using CheckSumConsole.Models;
using System;

namespace CheckSumConsole
{
    class Program
    {

        private Parameter Parameter = Parameter.Instance;

        static void Main(string[] args)
        {
            IZipFactory ZipFactory = ZipServiceFactory.Instance();

            string operation = Parameter.ParamValue(args, Parameter.OPERATION);
            switch (operation)
            {
                case Operation.ZIP:
                    {
                        string source = Parameter.ParamValue(args, Parameter.SOURCE);
                        string destination = Parameter.ParamValue(args, Parameter.DESTINATION);
                        if (source != null)
                            ZipFactory
                                 .GetZipService()
                                 .Zip(source, destination);
                        else
                            Console.WriteLine("Error please enter source and destination correctly");

                    }
                    break;
                case Operation.CHECKSUM:
                    {
                        string file1 = Parameter.ParamValue(args, Parameter.SOURCE);
                        string file2 = Parameter.ParamValue(args, Parameter.DESTINATION);
                        if (file1 != null && file2 != null)
                        {
                            bool areFilesCorrect = ZipFactory
                                                         .GetCheckSumService()
                                                         .CompareFromFile(file1, file2);
                            Console.WriteLine("File {0} and {1} {2}", file1, file2, areFilesCorrect ? "are the same" : "has corrupt");
                        }
                        else
                            Console.WriteLine("Error file path are incorrect");
                        Console.ReadKey();
                    }
                    break;
                case Operation.HELP:
                    Console.WriteLine("helping..");
                    break;
                default:
                    Console.WriteLine("Not a valid operation");
                    Screen.Help();
                    break;
            }
        }

    }
}

