﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSumConsole
{
    public class Screen
    {
        public static void Help()
        {
            Console.WriteLine("Help");
            Console.WriteLine(" _________________________________________________________________________");
            Console.WriteLine("|                             Useful commands                             |");
            Console.WriteLine("|_________________________________________________________________________|");
            Console.WriteLine("| To execute an operation   -> operation:<name|bundlefiles|copy|checksum> |");
            Console.WriteLine("| Operations:                                                             |");
            Console.WriteLine("| 1.   To bundle a folder:  -> operation:bundlefiles                      |");
            Console.WriteLine("|                              s:<source folder>  d:<destination folder>  |");
            Console.WriteLine("| 2.   To manually check integrity of 2 file: -> operation:checksum       |");
            Console.WriteLine("|                              sf:<source file>  df:<destination file>    |");
            Console.WriteLine("| ________________________________________________________________________|");
        }
    }
}
