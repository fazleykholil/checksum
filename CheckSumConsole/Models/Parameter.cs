﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSumConsole.Models
{
    public sealed class Parameter
    {
        private static readonly Parameter instance = new Parameter();

        public const string DESTINATION = "DESTINATION:";
        public const string SOURCE = "SOURCE:";
        public const string SOURCE_FILE = "sf:";
        public const string DESTINATION_FILE = "df:";
        public const string OPERATION = "/OPERATION:";

        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }

        private Parameter() { }

        public static Parameter Instance
        {
            get
            {
                return instance;
            }
        }
        public static string ParamValue(string[] args, string param)
        {
            try
            {
                string paramPair = args.First(arg => arg.StartsWith(param));
                if (paramPair != null)
                    return paramPair.Substring(paramPair.IndexOf(":") + 1);
                else
                    return null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Not a valid command");
                return null;
            }
        }
    }
}
